'''
Send image JSON to server and get respose
'''

import argparse
import requests
from gcv_out_to_json import get_gcv_out_json

# Define the --img_path argument
parser = argparse.ArgumentParser()
parser.add_argument('--img_path', '-p', help='Path to input image',\
    type=str, required=True)

# Get image path from user
args = parser.parse_args()
image_path = args.img_path

print(f'[DEBUG] Read image: {image_path}')

# Server address
addr = "https://us-central1-receiptscanner-302711.cloudfunctions.net/my-gcp-function"

# Headers for http request
content_type = 'application/json'
headers = {'content-type': content_type}

# Get text_box_dict
print(f'[DEBUG] Running the GCV OCR API')
text_box_dict = get_gcv_out_json(img_path=image_path)

# Send request and receive response
print(f'[DEBUG] Sending request to GCP Function')
response = requests.post(addr, data=text_box_dict, headers=headers)

# Decode response
print(f'[INFO] Response:\n{response.text}')

print('DONE! ^ ^')
