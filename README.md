# **GET GCV output and send it to GCV Function**
This repository shows how to get Google Cloud Vision OCR API response given an image path, then convert it to JSON object and send it to GCP Function for post-processing.


## **Steps to run the algorithm in Python (demo)**
***


### **Step 0:** Installing libraries and dependencies
 - Install Google Cloud Platform SDK
 ```shell
 sh ./install_gcp_sdk.sh
 ```

 - Install Google Cloud Vision API

 - Make sure you have the API key and you added it to $PATH


### **Step 1:** Run the client
 - Run the GCP Function clien
 ```python
 python3 gcp_func_client.py --img_path /path/to/image/
 ```



## **Steps to run the algorithm in another language**
***


### **Step 0:** Installing libraries and dependencies
 - Install Google Cloud Platform SDK
 ```shell
 sh ./install_gcp_sdk.sh
 ```

 - Install Google Cloud Vision API

 - Make sure you have the API key and you added it to $PATH
 

### **Step 1:** Running the GCV OCR API
 - ***Case 1: using the GCV API***
   - Take a look at the Python code and replicate it in your preferred programming language
     **Note:** Make sure you are using the following functions:

        `response = client.text_detection(image=image)`

        and

        `gcv_texts = response.text_annotations` 

 - ***Case 2: using the GCV REST API***
    Check out [this section](https://cloud.google.com/vision/docs/ocr#detect_text_in_a_local_image)

 - For more info check out the [official documentation](https://cloud.google.com/vision/docs/ocr)


### **Step 2:** Convert the GCV respose to JSON object
 - Like in the `get_text_box` funtion, all you have to do is to iterate over the text and boxes in the GCV response and save them in a new dictionary. Afterwards, dump this dictionary into a JSON object.


### **Step 3:** Send the JSON object to GCP Function
 - Like in the `gcp_func_client.py` file, all you have to do now is to send the `text_box_dict` to the GCP Function using the address specified in file.


### <center> ***Done! ^ ^*** </center>