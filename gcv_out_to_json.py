'''
Get GCV OCR output and convert it to JSON
'''
import json
import io
from google.cloud import vision

client = vision.ImageAnnotatorClient()

def detect_text(img_path):
    '''
    Detects text in the file. and return the text with its bounding box

    Args:
        img_path (str): path to image

    Returns:
        text_box_dict (dict): dictionary of the text and
                            its respective bounding box
    '''
    # Read image as bytes
    with io.open(img_path, 'rb') as image_file:
        img_bytes = image_file.read()

    # Send image to GCV as a request
    image = vision.Image(content=img_bytes)

    # Run GCV OCR
    ### Main OCR functions ###
    # Debugger note: ignore any `client has no attribute text_detection`
    # error; it's just PyLint being PyLint
    response = client.text_detection(image=image)
    gcv_texts = response.text_annotations

    return gcv_texts


def get_text_box(gcv_texts):
    '''
    Get text and its respective bounding box coordinate

    Args:
        gcv_texts (RepeatedComposite): response text from GCV

    Returns:
        text_box_dict (dict): dict of text and boudning boxes
    '''
    text_box_dict = {}    # initialise the dict

    # Populate the text_box_dict
    for text in gcv_texts:
        vertices = [(vertex.x, vertex.y) for vertex in text.bounding_poly.vertices]
        text_box_dict[text.description] = vertices

    # Convert the dict to JSON object
    return json.dumps(text_box_dict)


def get_gcv_out_json(img_path):
    '''
    Main pipeline.

    Detect text using GCV OCR API.
    Convert the API output to dict.

    Args:
        img_path (str): path to image

    Returns:
        text_box_dict (json): JSON object of text and its respective bounding box
    '''
    gcv_out = detect_text(img_path)
    text_box_dict = get_text_box(gcv_out)
    return text_box_dict
